#include <iostream>
using namespace std;

    #define MAXB 1000 // Maksimalus lentos dydis
    #define UNASSIGNED false // Nepažymėtas langelis
    #define ASSIGNED true // Pažymėtas langelis

    int boardSize; // Lentelės dydis
    int rookCount; // Rikio kiekis
    bool board[MAXB + 10][MAXB + 10]; // Lenta, + 10 dėl viso pikta

    bool validCoordinate(int x, int y){ /// Ar galima koordinatė
        return !(x < 0 || y < 0 || x > boardSize || y > boardSize);
    }

    bool isSafe(int x, int y){ /// Ar rikis yra saugus taške x, y
        int i, j, k, newx, newy; // Kintamieji
        if(board[y][x]) return false;
        for(i = 1; i >= -1; i--){ // Vertikalios pozicijos
            for(j = 1; j >= -1; j--){ // Horizontalios pozicijos
                if(j != 0 && i != 0){ // Kad būtų įstiržai
                    for(k = 1; k < boardSize; k++){ // Per visas įstrižas koordinates
                        newx = x + (j * k); // Naujos koordinatës
                        newy = y + (i * k);
                        if(!validCoordinate(newx, newy)) break;
                        if(board[newy][newx]) return false; // Jei ten jau yra rikis
                    }
                }
            }
        }
        return true;
    }

    void displayBoard(){ /// Lentos išvedimas į ekraną
        int i, j; // Kintamieji
        for(i = 0; i < boardSize; i++){
            for(j = 0; j < boardSize; j++)
                cout << board[i][j];
            cout << endl;
        }
    }

    bool solveRook(int currentRook, int x, int y){ /// Rikiø rekursija
        int i, j; // Kintamieji
        if(currentRook == rookCount) return true;
        for(i = y; i < boardSize; i++){
            for(j = x; j < boardSize; j++){
                if(isSafe(j, i)){
                    board[i][j] = ASSIGNED;
                    if(solveRook(currentRook + 1, x, y))
                        return true;
                    board[i][j] = UNASSIGNED;
                }
            }
        }
        return false; // Gryžtam atgal vienu gyliu, t.y backtrackinam
    }

 int main(){

    setlocale(LC_ALL, "Lithuanian"); // Lietuviškos raidės consolėje

    cout << "Jei teisingai įvesi duomenis programa tikrai suras gerą atsakymą" << endl;
    cout << "Jei įvesi per didelius duomenis, gali neužtekti kompiuteriaus stacko, LOL" << endl << endl;
    // Nuskaitymai
    cout << "Įveskite lentos dydį: "; cin >> boardSize;
    cout << "Įveskite rikių kiekį: "; cin >> rookCount;

    solveRook(0, 0, 0);
    displayBoard();

 return 0; }
